package net.samagames;

import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * Created by Werter on 2016-12-26.
 * A class for enum all animation in the game
 */
public enum Animation {
    FIRST(new Location(Bukkit.getWorld("world"),1022,84,-1178),false),
    SECOND(new Location(Bukkit.getWorld("world"),1027,67,-1147),false),
    THIRD(new Location(Bukkit.getWorld("world"),1027,60,-632),false);

    private Location location;
    private boolean aBoolean;
    Animation(Location location, boolean aBoolean){
        this.location = location;
        this.aBoolean = aBoolean;
    }

    /**
     *
     * @return The location
     */
    public Location getLocation() {
        return location;
    }

    /**
     *
     * @return True if the animation was already played else false.
     */
    public boolean isPlayed(){
        return  this.aBoolean;
    }

    /**
     * Play the animation and set it played.
     */
    public void play(){
        this.aBoolean = true;
    }
}

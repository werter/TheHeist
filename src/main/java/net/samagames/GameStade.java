package net.samagames;

import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * Created by Werter on 2016-12-21.
 *  An enumeration for all stade of the game.
 */
public enum GameStade{
    HUB(new Location(Bukkit.getWorld("world"),1100,62,-1653,-180,0)),
    TUTORIAL(new Location(Bukkit.getWorld("world"),1026,59,-1180,90,5)),
    LEVEL_ONE(new Location(Bukkit.getWorld("world"),1027,64,-626,180,5));
    private Location location;
    GameStade(Location location){
        this.location = location;
    }
    public Location getLocation(){
        return this.location;
    }
}
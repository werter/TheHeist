package net.samagames;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Werter on 2016-12-21.
 *
 *  <p>
 *    A new builder for ItemStack.
 *  </p>
 * @see org.bukkit.inventory.ItemStack
 */
public class ItemStackBuilder {

    private ItemStack itemStack;

    public ItemStackBuilder(Material material, String name){
        this.itemStack = new ItemStack(material);
        ItemMeta itemMeta = this.itemStack.getItemMeta();
        itemMeta.setDisplayName(name);
        this.itemStack.setItemMeta(itemMeta);
    }

    public ItemStackBuilder(Material material, String name, String... lore){
        this.itemStack = new ItemStack(material);
        ItemMeta itemMeta = this.itemStack.getItemMeta();
        itemMeta.setDisplayName(name);
        List<String> listLore = new ArrayList<String>();
        for(String loreString : lore)
            listLore.add(loreString);
        itemMeta.setLore(listLore);
        this.itemStack.setItemMeta(itemMeta);

    }

    public ItemStackBuilder(Material material, String name,Enchantment enchantment, Integer level, String... lore){
        this.itemStack = new ItemStack(material);
        this.itemStack.addEnchantment(enchantment,level);
        ItemMeta itemMeta = this.itemStack.getItemMeta();
        itemMeta.setDisplayName(name);
        List<String> listLore = new ArrayList<String>();
        for(String loreString : lore)
            listLore.add(loreString);
        itemMeta.setLore(listLore);
        this.itemStack.setItemMeta(itemMeta);
    }

    /**
     *
     * @return The ItemStack
     */
    public ItemStack getItemStack(){
        return  this.itemStack;
    }
}

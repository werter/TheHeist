package net.samagames;

import net.samagames.command.CmdStart;
import net.samagames.entity.Camera;
import net.samagames.listener.PlayerInteractEvent;
import net.samagames.listener.PlayerJoinEvent;
import net.samagames.listener.PlayerMoveEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by Werter on 2016-12-21.
 */
public class TheHeist extends JavaPlugin {
    private static TheHeist instance;
    public static TheHeistGame theHeistGame;

    @Override
    public void onEnable() {
        instance = this;
        theHeistGame = new TheHeistGame();
        if(!getDataFolder().exists())
            getDataFolder().mkdir();

        //Load all camera
        File cfile = new File(getDataFolder(), "camera.yml");
        if (!cfile.isFile()) saveResource(cfile.getName(), false);
        Camera.load(YamlConfiguration.loadConfiguration(cfile));

        //Load all door
        File ldfile = new File(getDataFolder(), "lockedDoor.yml");
        if (!ldfile.isFile()) saveResource(ldfile.getName(), false);
        Camera.load(YamlConfiguration.loadConfiguration(ldfile));


        PluginManager pluginManager = Bukkit.getPluginManager();
        Bukkit.getPluginCommand("start").setExecutor(new CmdStart());
        pluginManager.registerEvents(new PlayerJoinEvent(), this.getInstance());
        pluginManager.registerEvents(new PlayerMoveEvent(), this.getInstance());
        pluginManager.registerEvents(new PlayerInteractEvent(), this.getInstance());
        theHeistGame.setGameStade(GameStade.HUB);

    }

    /**
     * @return The TheHeist plugin
     */
    public static TheHeist getInstance(){
        return instance;
    }

}

package net.samagames;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

/**
 * Created by Werter on 2016-12-21.
 */
public class TheHeistGame {
    private ArrayList<ArmorStand> armorStands = new ArrayList<ArmorStand>();
    private ItemStack recharger,hacker;
    private int hackerLevel,rechargerLevel;
    private boolean isStarted;
    private boolean playerIsInAnimation;
    private Player player;
    private int deathCount;
    private GameStade gameStade;

    public TheHeistGame (){
        this.hacker = new ItemStackBuilder(Material.DIRT,ChatColor.GREEN + "Hackeur",ChatColor.LIGHT_PURPLE +"Clique droit pour l'utiliser",ChatColor.LIGHT_PURPLE +"Cout: 15 exp.",ChatColor.YELLOW + "Niveau 1").getItemStack();
        this.recharger = new ItemStackBuilder(Material.STONE, ChatColor.BLUE + "Rechargeur", ChatColor.LIGHT_PURPLE +"Clique droit sur une balise pour recharger", ChatColor.LIGHT_PURPLE +"Recharge 1 exp par seconde",ChatColor.YELLOW + "Niveau 1").getItemStack();
        hackerLevel = 1;
        rechargerLevel = 1;
        this.setStarted(false);
        this.deathCount = 0;
    }

    /**
     *
     * @return All the armorStands
     */
    public ArrayList<ArmorStand> getArmorStands(){
        return this.armorStands;
    }
    /**
     *  Initialize all armorstand of the game.
     */
    public void init(){
        ArmorStand armorStand = player.getWorld().spawn(new Location(player.getWorld(),1103,63,-1653).subtract(-0.5,2,-0.5), ArmorStand.class);
        armorStand.setCustomName("Commencer");

        ArmorStand armorStandDisable = player.getWorld().spawn(new Location(player.getWorld(),1031,61,-623).subtract(-0.5,2,-0.5),ArmorStand.class);
        armorStandDisable.setCustomName("Désactiver la caméra");

        ArmorStand armorStandClear = player.getWorld().spawn(new Location(player.getWorld(),1029,61,-620).subtract(-0.5,2,-0.5),ArmorStand.class);
        armorStandClear.setCustomName("Effacé l'alert");
        this.armorStands.add(armorStandClear);
        this.armorStands.add(armorStandDisable);
        this.armorStands.add(armorStand);
        for(ArmorStand armorStandList : this.armorStands) {
            armorStandList.setGravity(false);
            armorStandList.setSmall(true);
            armorStandList.setVisible(true);
            armorStandList.setCustomNameVisible(true);
        }
    }
    /**
     *
     * @param playerIsInAnimation true if the player is actually playing an animation
     */
    public void setPlayerIsInAnimation(boolean playerIsInAnimation) {
        this.playerIsInAnimation = playerIsInAnimation;
    }

    /**
     *
     * @return true if player is actually playing an animation else false.
     */
    public boolean isPlayerIsInAnimation() {
        return playerIsInAnimation;
    }

    /**
     *
     * @return the level of hacker item.
     */
    public int getHackerLevel(){
        return  this.hackerLevel;
    }

    /**
     *
     * @return the level of recharger item.
     */
    public int getRechargerLevel() {
        return rechargerLevel;
    }

    /**
     *
     * @return the recharger itemStack.
     */
    public ItemStack getRechargerItem(){
        return this.recharger;
    }

    /**
     *
     * @return the hacker itemStack
      */
    public ItemStack getHackerItem(){
        return this.hacker;
    }
    /**
     *
     * @return the player.
     */
    public Player getPlayer(){
        return  this.player;
    }

    /**
     *
     * @param player the player play.
     */
    public void setPlayer(Player player){
        this.player = player;
    }

    /**
     * Add 1 to the deathcount and teleport the player to the DeathRoom.
     */
    public void death(){
        this.deathCount++;
        Location deathLocation = new Location(player.getWorld(),1013,97,-645);
        player.setExp(0);
        player.sendTitle(ChatColor.YELLOW + "Attrapé",ChatColor.RED +"Vous vous êtes fait attrapé ésseyez encore une fois");
        player.teleport(deathLocation);
        player.sendMessage(ChatColor.BOLD + "" + ChatColor.GRAY + "Vous êtes mort " + ChatColor.RED + getDeathCount());
        new BukkitRunnable(){
            public void run() {
               player.teleport(getGameStade().getLocation());
            }
        }.runTaskLater(TheHeist.getInstance(),60);
    }
    /**
     * @return true if the game has started
     */
    public boolean isStarted(){
        return this.isStarted;
    }

    /**
     *
     * @param isStarted set the game is started or not.
     */
    public void setStarted(Boolean isStarted){
        this.isStarted = isStarted;
    }

    /**
     *
     * @param gameStade set the stade of the game.
     */
    public void setGameStade(GameStade gameStade){
        this.gameStade = gameStade;
    }

    /**
     *
     * @return the stade of the game.
     */
    public GameStade getGameStade(){
        return this.gameStade;
    }

    /**
     *
     * @return The number of time the player death
     */
    public int getDeathCount(){
        return this.deathCount;
    }
}

package net.samagames.command;

import net.samagames.GameStade;
import net.samagames.TheHeist;
import net.samagames.TheHeistGame;
import net.samagames.entity.Camera;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;


/**
 * Created by Werter on 2016-12-21.
 */
public class CmdStart implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        final TheHeistGame theHeist = TheHeist.theHeistGame;

     if(theHeist.isStarted())
            sender.sendMessage(ChatColor.RED + "La partie a déjà commencé");
        else if (sender instanceof  Player){
             final Player player = (Player) sender;


            theHeist.setPlayer(player);

            theHeist.setGameStade(GameStade.TUTORIAL);
           /* List<Location> locationList = new ArrayList<Location>();
            Location location1 = new Location(player.getWorld(),player.getLocation().getBlockX(),player.getLocation().getBlockY(),player.getLocation().getBlockZ());
            Location location2 = new Location(player.getWorld(),location1.getBlockX(),location1.getBlockY(),location1.getBlockZ(),90,5);
            Location location3 = new Location(player.getWorld(),location1.getBlockX(),location1.getBlockY(),location1.getBlockZ(),-50,5);

            locationList.add(location1);
            locationList.add(location2);
            locationList.add(location3);
            Camera camera   = new Camera(player.getLocation().add(0,3,0),10,player.getLocation().getBlockY()-1);
            camera.init();
            camera.enable();
            for(Location loc :camera.getFloor())
                loc.getBlock().setType(Material.STONE);*/
            player.sendMessage(ChatColor.YELLOW  + "La partie commence.");

            theHeist.setStarted(true);
            ItemStack hacker = theHeist.getHackerItem();
            ItemStack recharger = theHeist.getRechargerItem();

            player.getInventory().clear();
            player.getInventory().addItem(recharger);
            player.getInventory().addItem(hacker);

            List<String> messageList = new ArrayList<String>();
            messageList.add(ChatColor.YELLOW + "Clique avec l'item " + ChatColor.GREEN + "\"Hackeur\"" + ChatColor.YELLOW + "sur les ordinateurs rouges et les codes pour pouvoir les hacker.");
            messageList.add(ChatColor.YELLOW + "Ton exp représente l'énergie, au niveau un le hackeur dépense 15 exp par utilisations !");
            messageList.add(ChatColor.YELLOW + "Pour recharger ton exp utilise le " + ChatColor.BLUE + "\"Rechargeur\"" + ChatColor.YELLOW +" en cliquant sur les balises de recharge.");
            final Iterator iterator = messageList.iterator();
            new BukkitRunnable() {
                public void run() {
                    player.playSound(player.getLocation(), Sound.BLOCK_REDSTONE_TORCH_BURNOUT,1,1);
                    player.sendMessage(iterator.next().toString());
                    if(!iterator.hasNext()) {
                        Location location = new Location(player.getWorld(),1103,63,-1653);
                        Block block = player.getWorld().getBlockAt( location);
                        block.setType(Material.RED_SANDSTONE_STAIRS);
                        block.setData((byte)2);
                        theHeist.init();
                        this.cancel();
                    }
                }
            }.runTaskTimer(TheHeist.getInstance(),20,20);
        }
        return false;
    }
}

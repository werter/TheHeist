package net.samagames.entity;

import net.samagames.GameStade;
import net.samagames.TheHeist;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;
import java.util.logging.Level;

/**
 * Created by Werter on 2016-12-22.
 * <p>
 *     A simple camera object.
 * </p>
 */
public class Camera{
    private static ArrayList<Camera> cameras = new ArrayList<Camera>();

    private ArmorStand armorStand;
    private Location location;
    private boolean isMoving;
    private boolean isEnable;
    private Integer floorHeight;
    private int range;
    private List<Location> direction;
    private HashMap<Location,List<Location>> floor = new HashMap<Location,List<Location>>();

    /**
     *
     * @param location The location were the camera is going to be.
     * @param range The distance maximal for see the player
     * @param floorHeight The height of the floor.
     * @param direction All direction (yaw,pitch).
     */
    public Camera (Location location, Integer range,Integer floorHeight ,List<Location> direction){
        this.location = location;
        this.isMoving = true;
        this.floorHeight = floorHeight;
        this.range = range;
        this.direction = direction;
    }
    public Camera (Location location, Integer range,Integer floorHeight){
        this.location = location;
        this.isMoving = false;
        this.floorHeight = floorHeight;
        this.range = range;
    }

    public static ArrayList<Camera> getCameras(){
        return cameras;
    }

    /**
     *
     * Init and enable all cameras
     */
    public static void load(FileConfiguration config){
        for (String name : config.getKeys(false)) {
            World world = Bukkit.getWorld("world");
            double x = config.getDouble(name+".x");
            double y = config.getDouble(name+".y");
            double z = config.getDouble(name+".z");
            float yaw = Float.parseFloat(config.getString(name+".yaw"));
            float pitch = Float.parseFloat(config.getString(name+".pitch"));
            int range = config.getInt(name+".range");
            int floorHeight= config.getInt(name+".floorHeight");
            Camera camera;
            new Location(world,x,y,z).getBlock().setType(Material.AIR);
            if(config.isList(name+".direction")){
                List<Location> direction = (List<Location>) config.getList(name+".direction");
                camera = new Camera(new Location(world,x,y,z,yaw,pitch),range,floorHeight,direction);
            }else
                camera =new Camera(new Location(world,x,y,z,yaw,pitch),range,floorHeight);
            camera.init();
            camera.enable();
            cameras.add(camera);
        }
    }
    /**
     *  Enable camera's surveillance
     */
    public void enable() {

        this.isEnable = true;
        if (!isMoving()) {
            BukkitRunnable bukkitRunnable = new BukkitRunnable() {
                public void run() {
                    Player player = TheHeist.theHeistGame.getPlayer();
                    if (!isEnable())
                        this.cancel();
                    if(TheHeist.theHeistGame.getGameStade() != GameStade.HUB && player != null)
                        for (Location locationFloor : getFloor())
                            if (locationFloor.getBlockX() == player.getLocation().getBlockX() && locationFloor.getBlockZ() == player.getLocation().getBlockZ()) {
                                 player.playSound(player.getLocation(), Sound.BLOCK_DISPENSER_LAUNCH,(float)1,(float)1);
                                 player.setExp(player.getExp() + (float) 0.01);
                              if (player.getExp() > (float) 1) {
                                  TheHeist.theHeistGame.death();
                                  break;
                              }

                         }
                }
            };
            bukkitRunnable.runTaskTimer(TheHeist.getInstance(), 10, 5);
        }else{
            new BukkitRunnable(){
                int count = 0;
                public void run() {
                    if(!isEnable())
                        this.cancel();
                    armorStand.teleport(direction.get(count));
                    if(count == direction.size()-1)
                        count = 0;
                    else
                        count++;
                }
            }.runTaskTimer(TheHeist.getInstance(), 30, 30);
            new BukkitRunnable(){
                public void run() {
                    Player player = TheHeist.theHeistGame.getPlayer();
                    if(TheHeist.theHeistGame.getGameStade() != GameStade.HUB && player != null) {
                            for (Location locationFloor : getFloor())
                                if (locationFloor.getBlockX() == player.getLocation().getBlockX() && locationFloor.getBlockZ() == player.getLocation().getBlockZ()) {
                                    player.playSound(player.getLocation(), Sound.BLOCK_DISPENSER_LAUNCH,(float)1,(float)1);
                                    player.setExp(player.getExp() + (float) 0.01);
                                    if (player.getExp() > (float) 1) {
                                        TheHeist.theHeistGame.death();
                                        break;
                                    }

                                }
                    }
                }
            }.runTaskTimer(TheHeist.getInstance(), 10, 5);

        }
    }

    /**
     *  Disable camera's surveillance
     */
    public void disable(){
        this.isEnable = false;
        new BukkitRunnable(){
            public void run() {
                if(isEnable())
                    this.cancel();
                Bukkit.getWorld("world").playEffect(location,Effect.FIREWORKS_SPARK,2,1);
            }
        }.runTaskTimer(TheHeist.getInstance(),5,5);
    }
    /**
     * init the camera's surveillance
     */
    public void init(){
        if(!isMoving()){
            armorStand = Bukkit.getWorld("world").spawn(location.subtract(0,1,0),ArmorStand.class);
            armorStand.setHelmet(new ItemStack(Material.SKULL_ITEM));
            armorStand.setVisible(false);
            armorStand.setSmall(true);
            armorStand.setGravity(false);
            try {
                List<Location> locationList = new ArrayList<Location>();
                int yaw = -20;
                while(yaw != 20){
                    yaw = yaw + 5;
                    Location locationYaw = location;
                    locationYaw.setYaw(location.getYaw()+yaw);
                    armorStand.teleport(locationYaw);
                    for (Block block : armorStand.getLineOfSight((Set<Material>) null, this.range)) {
                        int x = block.getLocation().getBlockX();
                        int z = block.getLocation().getBlockZ();
                        Location locationFloor = new Location(Bukkit.getWorld("world"), x, floorHeight, z);
                        locationList.add(locationFloor.getBlock().getLocation());                        }
                }
                armorStand.teleport(location);
                this.floor.put(location,locationList);
                 } catch (Exception e) {
                e.printStackTrace();
            }


        }else{
            armorStand = Bukkit.getWorld("world").spawn(location.subtract(0,1,0),ArmorStand.class);
            armorStand.setHelmet(new ItemStack(Material.SKULL_ITEM));
            armorStand.setVisible(false);
            armorStand.setSmall(true);
            armorStand.setGravity(false);

            for(Location loc : this.direction){
                armorStand.teleport(loc);
                try{
                    List<Location> locationList = new ArrayList<Location>();
                    int yaw = -20;
                    while(yaw != 20){
                        yaw = yaw + 5;
                        Location locationYaw = location;
                        locationYaw.setYaw(location.getYaw()+yaw);
                        armorStand.teleport(locationYaw);
                        for (Block block : armorStand.getLineOfSight((Set<Material>) null, this.range)) {
                            int x = block.getLocation().getBlockX();
                            int z = block.getLocation().getBlockZ();
                            Location locationFloor = new Location(Bukkit.getWorld("world"), x, floorHeight, z);
                            if(!locationFloor.add(0,1,0).getBlock().getType().equals(Material.AIR))
                                break;
                            else
                                locationList.add(locationFloor.getBlock().getLocation().subtract(0,1,0));                        }
                    }
                    armorStand.teleport(location);
                    this.floor.put(loc,locationList);

                }catch (Exception e){
                    Bukkit.getLogger().log(Level.WARNING, e.getMessage());
                }

            }
        }
        armorStand.getEyeLocation().getBlock().setType(Material.BARRIER);
    }

    /**
     *
     * @return The location of the camera.
     */
    public Location getLocation() {
        return location;
    }

    /**
     *
     * @return All the block the camera is acutally surveilling
     */
    public List<Location> getFloor(){
        if(!isMoving())
            return this.floor.get(this.location);
        else
            return this.floor.get(armorStand.getLocation());
    }
    /**
     *
     * @return true is camera is enable else false.
     */
    public boolean isEnable(){
        return this.isEnable;
    }

    /**
     *
     * @return true is camera is moving else false
     */
    public boolean isMoving(){
        return this.isMoving;
    }
}

package net.samagames.entity;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.material.Door;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by werter on 15.01.2017.
 *  <p>
 *      The object for LockedDoor.
 *  </p>
 */
public class LockedDoor implements Listener{

    public static final List<LockedDoor> lockedDoors = new ArrayList<LockedDoor>();

    private boolean isLocked;
    private Location unLocker;
    private Location location;

    /**
     *
     * @param isLocked true if the door is locked else false
     * @param doorLocation location of the door
     * @param unLocker location of thze block for unlock de door
     */
    public LockedDoor(boolean isLocked,Location doorLocation,Location unLocker){
        this.isLocked = isLocked;
        this.unLocker = unLocker;
        this.location = doorLocation;
    }

    public static void load(YamlConfiguration config){
        for (String name : config.getKeys(false)) {
            Material material = Material.valueOf(config.getString(name+".material"));

            Double xLocation = config.getDouble(name+".xLocation");
            Double yLocation = config.getDouble(name+".yLocation");
            Double zLocation = config.getDouble(name+".zLocation");
            BlockFace blockFace = BlockFace.valueOf(config.getString(name+"blockFace"));

            Double xUnLocker = config.getDouble(name+".xUnLocker");
            Double yUnLocker = config.getDouble(name+".yUnLocker");
            Double zUnLocker = config.getDouble(name+".zUnLocker");

            Boolean isLocked = config.getBoolean(name+".isLocked");

            Location locationDoor = new Location(Bukkit.getWorld("world"),xLocation,yLocation,zLocation);
            Location locationUnLocker = new Location(Bukkit.getWorld("world"),xUnLocker,yUnLocker,zUnLocker);

            LockedDoor lockedDoor = new LockedDoor(isLocked,locationDoor,locationUnLocker);
            lockedDoors.add(lockedDoor);
            locationDoor.getBlock().setType(material);
            ((Door) locationDoor.getBlock()).setFacingDirection(blockFace);
        }
    }

    /**
     *
     * @return true if the door is locked else false
     */
    public boolean isLocked(){
        return this.isLocked;
    }

    /**
     *
     * @param locked true for set the door locked else false
     */
    public void setLocked(boolean locked) {
        isLocked = locked;
    }

    /**
     *
     * @return The location of the door
     */
    public Location getLocation() {
        return location;
    }

    /**
     *
     * @return The location of the block to right click for unloack the door
     */
    public Location getUnLocker() {
        return unLocker;
    }

    /**
     *
     * @param location The location of the lockedDoor
     * @return LockedDoor with the location
     */
    public static LockedDoor getLockedDoor(Location location) {
        LockedDoor lockedDoor = null;
        for (LockedDoor lockedDoorList : lockedDoors)
            if (lockedDoorList.getLocation().getBlock().getLocation().equals(location)){
                lockedDoor = lockedDoorList;
                break;
             }
             return lockedDoor;
    }

    @EventHandler
    public void onPlayerInteractEvent(PlayerInteractEvent event){
        if(event.hasBlock() && event.getClickedBlock().getType().name().contains("DOOR")){
            Block door = event.getClickedBlock();
            LockedDoor lockedDoor = getLockedDoor(door.getLocation());
            if(lockedDoor != null && lockedDoor.isLocked)
                event.setCancelled(true);
        }
    }
}

package net.samagames.listener;

import net.samagames.GameStade;
import net.samagames.TheHeist;
import net.samagames.TheHeistGame;
import net.samagames.entity.Camera;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Werter on 2016-12-25.
 */
public class PlayerInteractEvent implements Listener {
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerInteractEvent(org.bukkit.event.player.PlayerInteractEvent event) {
        final TheHeistGame theHeistGame = TheHeist.theHeistGame;
        if (event.hasBlock() && event.getClickedBlock() != null) {
            final Player player = event.getPlayer();
            final Block block = event.getClickedBlock();
            if(block.getType().equals(Material.WOOD_STAIRS) && player.getItemInHand().equals(theHeistGame.getRechargerItem())){
                //actually i have only one level but in the futur it will be updated.
                switch (theHeistGame.getRechargerLevel()){
                    case 1:
                        new BukkitRunnable() {
                            public void run() {
                                if(player.getLevel() >= 100 || player.getLocation().distance(block.getLocation()) > 4) {
                                    this.cancel();
                                    return;
                                }
                                player.setLevel(player.getLevel()+1);
                                }
                        }.runTaskTimer(TheHeist.getInstance(),1,1);
                        break;
                }
            }

            if (!theHeistGame.isPlayerIsInAnimation() && player.getItemInHand().equals(theHeistGame.getHackerItem()) && player.getLevel() >= 15 &&block.getType().equals(Material.RED_SANDSTONE_STAIRS)) {
                if(block.getLocation().equals(new Location(player.getWorld(),1029,61,-620))){
                   player.setExp(0);
                    for(ArmorStand armorStand : theHeistGame.getArmorStands())
                        if(armorStand.getLocation().getBlockX() == block.getLocation().getBlockX() && block.getLocation().getBlockZ() == armorStand.getLocation().getBlockZ())
                            armorStand.remove();
                }
                if(block.getLocation().equals(new Location(player.getWorld(),1031,61,-623))){
                    block.setType(Material.ACACIA_STAIRS);
                    block.setData((byte) 1);
                    for(Camera camera : Camera.getCameras())
                        if(camera.isEnable() && camera.getLocation().distance(new Location(player.getWorld(),1032,62,-626)) <= 3)
                            camera.disable();
                    for(ArmorStand armorStand : theHeistGame.getArmorStands())
                        if(armorStand.getLocation().getBlockX() == block.getLocation().getBlockX() && block.getLocation().getBlockZ() == armorStand.getLocation().getBlockZ())
                            armorStand.remove();
                }
                if(block.getLocation().equals(new Location(player.getWorld(),1103,63,-1653))) {
                    theHeistGame.setPlayerIsInAnimation(true);
                    final ArrayList<Location> locationArrayList = new ArrayList<Location>();
                    locationArrayList.add(new Location(player.getWorld(), 1057, 59, -1130, -180, 0));
                    locationArrayList.add(new Location(player.getWorld(), 1058, 59, -1149, 110, -25));
                    locationArrayList.add(new Location(player.getWorld(), 1054, 59, -1169, 115, 0));
                    locationArrayList.add(new Location(player.getWorld(), 1043, 59, -1181, 85, -30));
                    locationArrayList.add(GameStade.TUTORIAL.getLocation());
                    final Iterator iterator = locationArrayList.iterator();
                    //Sound map.001 is not repertoried
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "playsound map.001 player @a " + player.getLocation().getBlockX() + " " + player.getLocation().getBlockY() + " " + player.getLocation().getBlockZ() + " 100");
                    player.setGameMode(GameMode.SPECTATOR);
                    player.setFlySpeed(0);
                    player.getInventory().setHelmet(new ItemStack(Material.PUMPKIN));
                    new BukkitRunnable() {
                        public void run() {
                            if (!iterator.hasNext()) {
                                this.cancel();
                                player.getInventory().setHelmet(new ItemStack(Material.ENDER_STONE));
                                player.setLevel(100);
                                player.setGameMode(GameMode.ADVENTURE);
                                player.sendMessage(ChatColor.LIGHT_PURPLE + "Voix:" + ChatColor.RESET + " Ok. Allons y plus d'une fois de plus. Votre travail est de voler les recherches et les prototypes. Pour cela, vous devez entrer dans le batîment.C'est plutôt simple, pour commencer vous devez monter sur le toit en suite entrez par le system de ventilation. En suite descendez jusqu'au rez de chaussée");
                                player.sendMessage(ChatColor.LIGHT_PURPLE + "Voix:" + ChatColor.RESET + " A ce stade vous devrez sortir et chercher le systeme de sécurité. Si vous vous faites attrapé tout le batîment se fermera. Debrouillez vous pour que ça n'arrive pas. Rejoingnez moi a la sorti du batîment. Bon qu'est ce que vous attendez ? Commencez.");
                                theHeistGame.setPlayerIsInAnimation(false);
                                return;
                            }
                            player.teleport((Location) iterator.next());
                        }
                    }.runTaskTimer(TheHeist.getInstance(), 20, 160);
                }
            }
            if (!block.getType().equals(Material.WALL_SIGN.RED_SANDSTONE_STAIRS) &&player.getLevel() >= 15 && player.getItemInHand().equals(theHeistGame.getHackerItem()))
                player.sendMessage(ChatColor.RED + "No console");
            else
                player.sendMessage(ChatColor.RED+"Vous n'avez pas suffisement d'energies");
            if(!event.getClickedBlock().getType().name().contains("DOOR"))
                event.setCancelled(true);
        }else
            if(event.getPlayer().getItemInHand().equals(TheHeist.theHeistGame.getHackerItem()) && !event.hasBlock())
                event.getPlayer().sendMessage(ChatColor.RED + "Rien a hacker");
    }
}

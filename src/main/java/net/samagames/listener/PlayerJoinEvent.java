package net.samagames.listener;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


/**
 * Created by Werter on 2016-12-21.
 */
public class PlayerJoinEvent implements Listener {
    @EventHandler (priority = EventPriority.NORMAL)
    public void onPlayerJoinEvent(org.bukkit.event.player.PlayerJoinEvent event) {
        Player player = event.getPlayer();
        //Check if a party is actually playing
        if (Bukkit.getOnlinePlayers().size() > 1) {
            player.kickPlayer(ChatColor.RED + "La partie est déjà en cours !");
            return;
        }
        event.setJoinMessage(null);

        player.setResourcePack("http://download1675.mediafire.com/2t4j2zdbyitg/i6aia7aa1pg5a15/ressources.zip");

        player.getInventory().clear();
        player.getInventory().setHelmet(new ItemStack(Material.ENDER_STONE));

        Location spawnLocation = new Location(Bukkit.getWorld("world"),1100,62,-1653,-180,0);
        player.teleport(spawnLocation);

        player.setExp(0);
        player.setLevel(0);
        player.setGameMode(GameMode.ADVENTURE);
        player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION,Integer.MAX_VALUE,0));
        player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE,Integer.MAX_VALUE,0));
        player.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION,Integer.MAX_VALUE,0));

        player.sendMessage(ChatColor.YELLOW + "Ce jeu est la version plugin de la map The Heist.");
    }
}

package net.samagames.listener;

import net.samagames.Animation;
import net.samagames.GameStade;
import net.samagames.TheHeist;
import net.samagames.TheHeistGame;
import net.samagames.entity.Camera;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

/**
 * Created by Werter on 2016-12-26.
 */
public class PlayerMoveEvent implements Listener{
    @EventHandler (priority = EventPriority.NORMAL)
    public void onPlayerMoveEvent(org.bukkit.event.player.PlayerMoveEvent event){
        Player player = event.getPlayer();
        TheHeistGame theHeistGame = TheHeist.theHeistGame;
        if(player.isSneaking())
            for(Camera camera : Camera.getCameras())
                if(camera.getLocation().distance(player.getLocation()) < 5)
                    for(Location location : camera.getFloor())
                        player.sendBlockChange(location, Material.ACACIA_DOOR.WOOL,(byte)14);
        if(!theHeistGame.isPlayerIsInAnimation()) {
            if (!Animation.FIRST.isPlayed() && player.getLocation().distance(Animation.FIRST.getLocation()) <= 2) {
                Animation.FIRST.play();
                //Sound map.002 is not repertoried
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "playsound map.002 player @a " + player.getLocation().getBlockX() + " " + player.getLocation().getBlockY() + " " + player.getLocation().getBlockZ() + " 100");
                player.sendMessage(ChatColor.LIGHT_PURPLE + "Voix: " + ChatColor.RESET + "Vous êtes la ? Votre micro marche toujours ? C'est super, bon début pour un cambriolage. Je suppose que votre micro est cassé. Continuez a avancer.");
                return;
            }
            if (!Animation.SECOND.isPlayed() && player.getLocation().distance(Animation.SECOND.getLocation()) <= 2){
                Animation.SECOND.play();
                player.teleport(new Location(Bukkit.getWorld("world"),1027 ,62, -632));
                theHeistGame.setGameStade(GameStade.LEVEL_ONE);
                return;
            }
            if(!Animation.THIRD.isPlayed() && player.getLocation().distance(Animation.THIRD.getLocation()) <= 2){
                Animation.THIRD.play();
                //Sound map.101 is not repertoried
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "playsound map.101 player @a " + player.getLocation().getBlockX() + " " + player.getLocation().getBlockY() + " " + player.getLocation().getBlockZ() + " 100");
                player.sendMessage(ChatColor.LIGHT_PURPLE+"Voix: " + ChatColor.RESET +"Ok maintenant arrête toi ici ! Je suis sérieux ne bouge pas. Avant de te rendre n'importe où. Il y a une camera dans le coin. Ta barre d'exp représente le niveau de l'alarme, une fois a 100 tu as été attrapé. Donc ce que j'ai besoin que tu fasse c'est de courir aussi vite que tu peux dans la salle juste en face.");
                return;
            }
        }else
            event.setCancelled(true);
    }
}
